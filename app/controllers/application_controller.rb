class ApplicationController < ActionController::Base
	after_action :allow_iframe, only: :embed
protect_from_forgery with: :exception
  include SessionsHelper


private

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end
end

