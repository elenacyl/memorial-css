class ColorCustToUsers < ActiveRecord::Migration[5.1]
  def change

    add_column :users, :themecolor, :string
 add_column :users, :bgcolor, :string
 add_column :users, :innercolor, :string
 add_column :users, :headtextcolor, :string
 add_column :users, :textcolor, :string
 add_column :users, :buttontext, :string
add_column :users, :buttonbg, :string

  end
end
